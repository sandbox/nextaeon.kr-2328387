 -- SUMMARY --

Taxonomy Menu Form module create menu items on Taxonomy Term page easily. But 
those menu items cannot translate with i18n menu translation. This module
integrate Taxonomy Menu Form with i18n menu module for translate menu items.
With this module, You can translate menu item like normal menu items.

 -- REQUIREMENTS --

 * Taxonomy Menu Form
 * Internationalization (i18n)
